package controller;

import db.DatabaseHandler;

import java.sql.*;

public class TimetableController {


    public void listAllTimetables() {
        DatabaseHandler databaseHandler = new DatabaseHandler();
        Statement statement = databaseHandler.createStatement();

        try {
            ResultSet results = statement.executeQuery("SELECT * FROM timetable");

            while (results.next()) {
                int myTimetableId = results.getInt("id");
                String myTimetableWeekDay = results.getString("weekday");
                String myTimetableArrivalTime = results.getString("arrivalTime");
                int myTimetableLocationId = results.getInt("locationId");
                int myTimetableBusId = results.getInt("busId");
                String myTimetableCreationDate = results.getString("createdOn");

                System.out.println(myTimetableId+" "+myTimetableWeekDay+" "+myTimetableArrivalTime+" "+
                        myTimetableBusId+" "+myTimetableLocationId+" "+myTimetableCreationDate);

            }
            statement.close();
            databaseHandler.closeConnection();
        } catch (SQLException ex) {
            System.out.println(ex);
        }

    }


    public void createTimetable (int busId, int locationId, String weekDay, String arrivalTime) {
        DatabaseHandler databaseHandler = new DatabaseHandler();
        Statement statement = databaseHandler.createStatement();

        try {
            String newTimetable = "INSERT INTO timetable (weekDay, arrivalTime, locationId, busId) VALUES ("
                    + "\"" + weekDay + "\"," + "\"" + arrivalTime + "\"," + locationId + "," + busId + ")";

            statement.executeUpdate(newTimetable);

            statement.close();
            databaseHandler.closeConnection();

        }catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void findTimetableForBusNumber(String busNumber) {
        DatabaseHandler databaseHandler = new DatabaseHandler();
        Statement statement = databaseHandler.createStatement();
        try {
            String findTimetable = "\tSELECT * FROM timetable\n" +
                    "\tINNER JOIN buses on buses.id = timetable.busId\n" +
                    "\tWHERE buses.busNumber = \"" + busNumber + "\"";
            ResultSet busTimetable = statement.executeQuery(findTimetable);
            if (busTimetable.next()){
                String myBusWeekDay = busTimetable.getString("weekday");
                String myBusArrivalTime = busTimetable.getString("arrivalTime");
                int myBusLocationId = busTimetable.getInt("locationId");

                System.out.println("Here is timetable for bus number " + busNumber
                        + ": weekday - " + myBusWeekDay + ", arrival time - " + myBusArrivalTime
                        + " location - " + myBusLocationId);
            } else
                System.out.println("Bus or timetable does not exist");

            statement.close();
            databaseHandler.closeConnection();

        }catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void insertToTimetable(String name, String address, String phoneNr, int age, double height) {

        DatabaseHandler databaseHandler = new DatabaseHandler();
        try {
            Connection connector = databaseHandler.getDbConnection();
            String INSERTDRIVERSQL = "INSERT INTO timetable (fullName, address, phone, age, height)" +
                    "VALUES (?, ?, ?, ?, ?);";
            PreparedStatement insertTimetable = connector.prepareStatement(INSERTDRIVERSQL);
            insertTimetable.setString(1, name);
            insertTimetable.setString(2,address);
            insertTimetable.setString(3, phoneNr);
            insertTimetable.setInt(4,age);
            insertTimetable.setDouble(5, height);

            int result = insertTimetable.executeUpdate();

            if (result ==1) {
                System.out.println("driver added");
            } else {
                System.out.println("driver not added");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void updateDrivers(String address, String phoneNr, int id) {

        DatabaseHandler databaseHandler = new DatabaseHandler();
        try {
            Connection connector = databaseHandler.getDbConnection();
            String UPDATEDRIVERSQL = "UPDATE drivers SET address=?, phone=? WHERE id=?";
            PreparedStatement updateDriver = connector.prepareStatement(UPDATEDRIVERSQL);
            updateDriver.setString(1,address);
            updateDriver.setString(2, phoneNr);
            updateDriver.setDouble(3, id);

            int result = updateDriver.executeUpdate();

            if (result ==1) {
                System.out.println("driver updated");
            } else {
                System.out.println("driver not updated");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void deleteDrivers(int id) {

        DatabaseHandler databaseHandler = new DatabaseHandler();
        try {
            Connection connector = databaseHandler.getDbConnection();
            String DELETEDRIVERSQL = "DELETE FROM drivers WHERE id=?";
            PreparedStatement updateDriver = connector.prepareStatement(DELETEDRIVERSQL);
            updateDriver.setDouble(1, id);

            int result = updateDriver.executeUpdate();

            if (result ==1) {
                System.out.println("driver deleted");
            } else {
                System.out.println("driver not deleted");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
